﻿using System;
using System.Text;

using NetCoreServer;

using static Mz.Server.File.Client.Program;

namespace Mz.Server.File.Client.Commands.Download.Browse
{
    internal class BrowseClient : TcpClient
    {
        public BrowseClient(string ipAddress, int port) : base(ipAddress, port) { }

        protected override void OnConnected()
        {
            WriteLogEntry("Successfully connected to server!");
        }

        protected override void OnDisconnected()
        {
            Socket.Close();
            Socket.Dispose();
            Dispose();
            WriteLogEntry("Session was disconnected!");
        }

        protected override void OnReceived(byte[] buffer, long offset, long size)
        {
            switch (buffer[0])
            {
                case 100:
                {
                    WriteLogEntry("Server acknowledged the request " +
                        "(AUTHENTICATION/SET)!");
                    break;
                }
                case 104:
                {
                    var serializedFileContent = Encoding.UTF8.GetString(buffer,
                        (int)offset + 1, (int)size - 1);
                    foreach (var item in serializedFileContent.Split(':'))
                    {
                        WriteLogEntry(item);
                    }
                    break;
                }
                case 198:
                {
                    WriteLogEntry("A error occurred during authentication!",
                        LogSeverity.Error);
                    Disconnect();
                    break;
                }
                case 199:
                {
                    WriteLogEntry("A generic error occurred!",
                        LogSeverity.Error);
                    Disconnect();
                    break;
                }
            }
        }
    }
}
