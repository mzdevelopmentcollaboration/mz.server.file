﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Mz.Server.File.Client.Generic;

using static Mz.Server.File.Client.Program;

// ReSharper disable PossibleNullReferenceException

namespace Mz.Server.File.Client.Commands.Download.Browse
{
    internal class BrowseManager
    {
        private delegate void ConnectionMonitoring();

        private BrowseClient _browseClient;
        private event ConnectionMonitoring ConnectionWatcher;

        private readonly KeyValuePair<string, string>[] _commandCollection =
        {
            new ("list <path>", "Lists all items within a entered path."),
            new ("help", "Displays information about each command."),
            new ("exit", "Disconnects the session and ends the program.")
        };

        private async void OnConnectEvent()
        {
            while (_browseClient.IsConnected)
            {
                await Task.Delay(125);
            }
            Environment.Exit(1);
        }

        public void GetFileShareContents(string ipAddress, int port)
        {
            WriteLogEntry("Testing connection to the server...");
            var connectionManager = new ConnectionManager();
            if (!connectionManager.CheckNetworkConnectivity(ipAddress, port))
                return;
            WriteLogEntry("Please enter your authentication code: ",
                LogSeverity.Information, false);
            var enteredAuthenticationCode = string.Empty;
            while (true)
            {
                var enteredKey = Console.ReadKey(true);
                if (enteredKey.Key == ConsoleKey.Enter) break;
                enteredAuthenticationCode += enteredKey.KeyChar;
            }
            Console.WriteLine();
            var socketManager = new SocketManager();
            _browseClient = new BrowseClient(ipAddress, port);
            _browseClient.ConnectAsync();
            while (!_browseClient.IsConnected)
            {
                Thread.Sleep(125);
            }
            ConnectionWatcher += OnConnectEvent;
            ConnectionWatcher.Invoke();
            _browseClient.Send(socketManager
                .AuthenticateClient(enteredAuthenticationCode));
            Thread.Sleep(125);
            while (true)
            {
                if (!_browseClient.IsConnected) return;
                WriteLogEntry("> ",
                    LogSeverity.Information, false);
                var userInput = Console.ReadLine();
                if (userInput.StartsWith("list"))
                {
                    var requestedPath = string.Empty;
                    try
                    {
                        requestedPath = userInput.Substring(5);
                    }
                    catch
                    {
                        WriteLogEntry("No explicit path was entered. " +
                            "Showing root-entries.");
                    }
                    var requestedPathBytes = Encoding.UTF8.GetBytes(requestedPath);
                    var requestedPathBuffer = new byte[requestedPathBytes.Length + 1];
                    requestedPathBuffer[0] = 4;
                    for (var i = 0; i < requestedPath.Length; i++)
                    {
                        requestedPathBuffer[i + 1] = requestedPathBytes[i];
                    }
                    _browseClient.Send(requestedPathBuffer);
                }
                else if (userInput.StartsWith("help"))
                {
                    foreach (var (command, description) in _commandCollection)
                    {
                        WriteLogEntry(command + ": " + description);
                    }
                }
                else if (userInput.StartsWith("exit"))
                {
                    _browseClient.Disconnect();
                    break;
                }
                else
                {
                    WriteLogEntry("Input was not recognized as a command.");
                }
                Thread.Sleep(125);
            }
        }
    }
}
