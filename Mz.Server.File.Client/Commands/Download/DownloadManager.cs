﻿using CommandDotNet;

using Mz.Server.File.Client.Commands.Download.File;
using Mz.Server.File.Client.Commands.Download.Browse;

namespace Mz.Server.File.Client.Commands.Download
{
    [Command(Description = "Downloads a single file or a folder.")]
    public class DownloadManager
    {
        [Command(Description = "Downloads a single file based on an input-path.",
            Name = "file")]
        public void DownloadFile(string ipAddress, int port, string authenticationCode,
            string toBeReceivedFilePath, string targetFilePath)
        {
            new FileManager().DownloadFile(ipAddress, port,
            toBeReceivedFilePath, targetFilePath);
        }

        [Command(Description = "Browse the remote file-share.",
            Name = "browse")]
        public void BrowseFileShare(string ipAddress, int port)
        {
            new BrowseManager().GetFileShareContents(ipAddress, port);
        }
    }
}
