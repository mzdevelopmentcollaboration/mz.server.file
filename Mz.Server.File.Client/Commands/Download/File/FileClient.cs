﻿using System.IO;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;

using NetCoreServer;

using static Mz.Server.File.Client.Program;

// ReSharper disable NotAccessedField.Local
// ReSharper disable once PossibleNullReferenceException

namespace Mz.Server.File.Client.Commands.Download.File
{
    internal class FileClient : TcpClient
    {
        private delegate void FileWatcher();

        private FileStream _fileStream;
        private bool _fileTransferStarted;
        private long _toBeReceivedFileSize;
        private readonly string _targetFilePath;
        private event FileWatcher FileWatcherEvent;
        private readonly string _toBeReceivedFilePath;
        private readonly Stopwatch _stopwatch = new();

        public FileClient(string address, int port, string toBeReceivedFilePath,
            string targetFilePath) : base(address, port)
        {
            _targetFilePath = targetFilePath;
            _toBeReceivedFilePath = toBeReceivedFilePath;
            FileWatcherEvent += OnFileWatcherEvent;
            if (System.IO.File.Exists(_targetFilePath))
                System.IO.File.Delete(_targetFilePath);
        }

        private bool FileTransferStarted
        {
            get => _fileTransferStarted;
            set
            {
                _fileTransferStarted = value;
                FileWatcherEvent.Invoke();
            }
        }

        private async void OnFileWatcherEvent()
        {
            _stopwatch.Start();
            while (new FileInfo(_targetFilePath).Length < _toBeReceivedFileSize)
            {
                WriteLogEntry("Download is in progress! " +
                    $"Downloaded {new FileInfo(_targetFilePath).Length} Bytes " +
                    $"of {_toBeReceivedFileSize}!");
                await Task.Delay(250);
            }
            _stopwatch.Stop();
            _fileStream.SetLength(_toBeReceivedFileSize);
            WriteLogEntry("Finished transfer in " +
                $"{_stopwatch.Elapsed.Seconds} seconds!");
            Disconnect();
        }

        protected override void OnConnected()
        {
            WriteLogEntry("Successfully connected to server!");
            _fileStream = new FileStream(_targetFilePath, FileMode.Append);
        }

        protected override void OnDisconnected()
        {
            _fileStream.Close();
            _fileStream.Dispose();
            Socket.Close();
            Socket.Dispose();
            Dispose();
            WriteLogEntry("Session was disconnected!");
        }

        protected override void OnReceived(byte[] buffer, long offset, long size)
        {
            if (!FileTransferStarted)
            {
                switch (buffer[0])
                {
                    case 100:
                    {
                        WriteLogEntry("Server acknowledged the request " +
                            "(AUTHENTICATION/SET)!");
                        break;
                    }
                    case 101:
                    {
                        _toBeReceivedFileSize = long.Parse(Encoding.UTF8.GetString(buffer,
                            (int)offset + 1, (int)size));
                        WriteLogEntry("Server returned a file-size of " +
                            $"{_toBeReceivedFileSize} Bytes!");
                        FileTransferStarted = true;
                        break;
                    }
                    case 198:
                    {
                        WriteLogEntry("A error occurred during authentication!",
                            LogSeverity.Error);
                        if (System.IO.File.Exists(_targetFilePath))
                            System.IO.File.Delete(_targetFilePath);
                        Disconnect();
                        break;
                    }
                    case 199:
                    {
                        WriteLogEntry("A generic error occurred!",
                            LogSeverity.Error);
                        if (System.IO.File.Exists(_targetFilePath))
                            System.IO.File.Delete(_targetFilePath);
                        Disconnect();
                        break;
                    }
                }
                return;
            }
            _fileStream.Write(buffer, (int)offset, (int)size);
        }
    }
}
