﻿using System;
using System.Threading;

using NetCoreServer;

using Mz.Server.File.Client.Generic;

using static Mz.Server.File.Client.Program;

// ReSharper disable MemberCanBeMadeStatic.Local

namespace Mz.Server.File.Client.Commands.Download.File
{
    internal class FileManager
    {
        public void DownloadFile(string ipAddress, int port,
                string toBeReceivedFilePath, string targetFilePath)
        {
            WriteLogEntry("Please enter your authentication code: ",
                LogSeverity.Information, false);
            var enteredAuthenticationCode = Console.ReadLine();
            using var client = new FileClient(ipAddress, port,
                toBeReceivedFilePath, targetFilePath);
            client.ConnectAsync();
            while (!client.IsConnected)
            {
                Thread.Sleep(125);
            }
            InitializeFileDownload(client,
                toBeReceivedFilePath, enteredAuthenticationCode);
            do
            {
                Thread.Sleep(125);
            }
            while (client.IsConnected);
        }

        private void InitializeFileDownload(TcpClient fileClient,
            string toBeReceivedFilePath, string authenticationCode)
        {
            var socketManager = new SocketManager();
            for (var i = 0; i <= 3; i++)
            {
                if (!fileClient.IsConnected) return;
                switch (i)
                {
                    case 0:
                    {
                        fileClient.Send(socketManager.AuthenticateClient(
                            authenticationCode));
                        break;
                    }
                    case 1:
                    {
                        fileClient.Send(socketManager
                            .SetRequestedFile(toBeReceivedFilePath));
                        break;
                    }
                    case 2:
                    {
                        fileClient.Send(socketManager.RequestFileLength());
                        break;
                    }
                    case 3:
                    {
                        fileClient.Send(socketManager.RequestFile());
                        break;
                    }
                    default:
                    {
                        WriteLogEntry("A unreachable phase has somehow been reached!",
                            LogSeverity.Error);
                        break;
                    }
                }
                Thread.Sleep(125);
            }
        }
    }
}
