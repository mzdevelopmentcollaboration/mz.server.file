﻿using System.Net;
using System.Net.Sockets;

using static Mz.Server.File.Client.Program;

namespace Mz.Server.File.Client.Generic
{
    internal class ConnectionManager
    {
        public bool CheckNetworkConnectivity(string ipAddress, int port)
        {
            using var tcpClient = new TcpClient();
            try
            {
                tcpClient.Connect(
                    IPAddress.Parse(ipAddress), port);
                return true;
            }
            catch
            {
                WriteLogEntry("Could not connect to the server!",
                    LogSeverity.Error);
                return false;
            }
        }
    }
}
