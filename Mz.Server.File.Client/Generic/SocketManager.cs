﻿using System.Text;

namespace Mz.Server.File.Client.Generic
{
    internal class SocketManager
    {
        public byte[] AuthenticateClient(string authenticationCode)
        {
            var authenticationCodeBytes = Encoding.UTF8
                .GetBytes(authenticationCode);
            var authenticationBuffer = new byte[authenticationCode.Length + 1];
            authenticationBuffer[0] = 0;
            for (var i = 0; i < authenticationCode.Length; i++)
            {
                authenticationBuffer[i + 1] = authenticationCodeBytes[i];
            }
            return authenticationBuffer;
        }

        public byte[] RequestFileLength()
        {
            return new byte[] { 2 };
        }

        public byte[] SetRequestedFile(string toBeReceivedFile)
        {
            var filePath = Encoding.UTF8
                .GetBytes(toBeReceivedFile);
            var filePathBuffer = new byte[filePath.Length + 1];
            filePathBuffer[0] = 1;
            for (var i = 0; i < filePath.Length; i++)
            {
                filePathBuffer[i + 1] = filePath[i];
            }
            return filePathBuffer;
        }

        public byte[] RequestFile()
        {
            return new byte[] { 3 };
        }
    }
}
