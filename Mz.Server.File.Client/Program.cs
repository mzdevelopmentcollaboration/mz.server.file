﻿using System;

using CommandDotNet;

using Mz.Server.File.Client.Commands.Download;

// ReSharper disable SwitchStatementHandlesSomeKnownEnumValuesWithDefault

namespace Mz.Server.File.Client
{
    internal class Program
    {
        public enum LogSeverity
        {
            Information,
            Warning,
            Error
        }

        public static void WriteLogEntry(string message,
            LogSeverity severity = LogSeverity.Information,
            bool writeLine = true)
        {
            if (writeLine)
            {
                switch (severity)
                {
                    case LogSeverity.Information:
                        Console.WriteLine($"[{DateTime.Now} Information] {message}");
                        break;
                    case LogSeverity.Warning:
                        Console.WriteLine($"[{DateTime.Now} Warning] {message}");
                        break;
                    case LogSeverity.Error:
                        Console.WriteLine($"[{DateTime.Now} Error] {message}");
                        break;
                }
                return;
            }
            switch (severity)
            {
                case LogSeverity.Information:
                    Console.Write($"[{DateTime.Now} Information] {message}");
                    break;
                case LogSeverity.Warning:
                    Console.Write($"[{DateTime.Now} Warning] {message}");
                    break;
                case LogSeverity.Error:
                    Console.Write($"[{DateTime.Now} Error] {message}");
                    break;
            }
        }

        private static void Main(string[] args)
        {
            new AppRunner<DownloadManager>().Run(args);
        }
    }
}
