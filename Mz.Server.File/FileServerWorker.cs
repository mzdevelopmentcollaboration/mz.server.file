using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;

using Mz.Server.File.Models;
using Mz.Server.File.Server;

namespace Mz.Server.File
{
    public class FileServerWorker : BackgroundService
    {
        private readonly FileServer _fileServer;
        private readonly Configuration _configuration = new();

        public FileServerWorker(IConfiguration configuration)
        {
            configuration.Bind(_configuration);
            _fileServer = new FileServer(IPAddress.Any, 4444,
                _configuration.Locale);
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _fileServer.Start();
            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                GC.Collect();
                await Task.Delay(5000, stoppingToken);
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _fileServer.Stop();
            return base.StopAsync(cancellationToken);
        }
    }
}
