﻿namespace Mz.Server.File.Models
{
    internal class Configuration
    {
        public SerilogConfiguration Serilog { get; set; }
        public LogEntries Locale { get; set; }

        public class SerilogConfiguration
        {
            public class MinimumLevel
            {
                public string Default { get; set; }
            }
        }

        public class LogEntries
        {
            public GenericEntries Generic { get; set; }
            public FileServerEntries FileServer { get; set; }
            public FileSessionEntries FileSession { get; set; }

            public class GenericEntries
            {
                public string Error { get; set; }
            }

            public class FileServerEntries
            {
                public string SessionDisposed { get; set; }
            }

            public class FileSessionEntries
            {
                public string Connected { get; set; }
                public string Disconnected { get; set; }
                public string Authenticated { get; set; }
                public string FileEntered { get; set; }
                public string LengthRequested { get; set; }
                public string FileRequested { get; set; }
                public string DisconnectForced { get; set; }
            }
        }
    }
}
