using System.IO;
using System.Reflection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

using Serilog;
using Serilog.Core;

// ReSharper disable AssignNullToNotNullAttribute

namespace Mz.Server.File
{
    public class Program
    {
        public static Logger Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .Enrich.FromLogContext()
            .WriteTo.Console()
            .CreateLogger();

        public static void Main(string[] args)
        {
            var sharedDirectory = Path.Combine(Path
                .GetDirectoryName(Assembly
                    .GetExecutingAssembly().Location),
                "Share");
            if (!Directory.Exists(sharedDirectory))
                Directory.CreateDirectory(sharedDirectory);
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging(config =>
                {
                    config.ClearProviders();
                })
                .ConfigureServices(services =>
                {
                    services.AddHostedService<FileServerWorker>();
                });
    }
}
