﻿using System.Net;
using System.Net.Sockets;

using NetCoreServer;

using Mz.Server.File.Models;
using Mz.Server.File.Session;

using static Mz.Server.File.Program;

namespace Mz.Server.File.Server
{
    internal class FileServer : TcpServer
    {
        private readonly Configuration.LogEntries _locale;

        public FileServer(IPAddress serverIpAddress, int serverPort,
            Configuration.LogEntries locale) :
            base(serverIpAddress, serverPort)
        {
            OptionSendBufferSize = 4096;
            _locale = locale;
        }

        protected override TcpSession CreateSession()
        {
            return new FileSession(this, _locale);
        }

        //TODO: Find out if this does anything of value.
        protected override void OnDisconnected(TcpSession session)
        {
            session.Dispose();
            Logger.Information(session.Id + _locale
                .FileServer.SessionDisposed);
        }

        protected override void OnError(SocketError socketError)
        {
            Logger.Error(_locale.Generic.Error + socketError);
        }
    }
}
