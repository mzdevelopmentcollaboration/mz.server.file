﻿using System;
using System.IO;
using System.Text;
using System.Reflection;
using System.Net.Sockets;

using NetCoreServer;

using Mz.Server.File.Models;

using static Mz.Server.File.Program;

// ReSharper disable AssignNullToNotNullAttribute
// ReSharper disable once PossibleNullReferenceException

namespace Mz.Server.File.Session
{
    internal class FileSession : TcpSession
    {
        private string _requestedFile;
        private bool _isNotAuthenticated;
        private readonly string _rootPath;
        private readonly string _authenticationCode;
        private readonly Configuration.LogEntries _locale;
        private readonly FileSessionManager _fileSessionManager;

        public FileSession(TcpServer fileServer, Configuration.LogEntries locale) :
            base(fileServer)
        {
            _isNotAuthenticated = true;
            _authenticationCode = Environment
                .GetEnvironmentVariable("fileServerAuthenticationCode");
            _rootPath = Path.Combine(Path.GetDirectoryName(
                Assembly.GetExecutingAssembly().Location), "Share");
            _fileSessionManager = new FileSessionManager();
            _locale = locale;
        }

        protected override void OnConnected()
        {
            Logger.Information(Id + _locale.FileSession.Connected);
        }

        protected override void OnDisconnected()
        {
            Logger.Information(Id + _locale.FileSession.Disconnected);
        }

        protected override void OnError(SocketError socketError)
        {
            Logger.Error(_locale.Generic.Error + socketError);
        }

        private void DisconnectForcefully(byte[] statusCode)
        {
            Logger.Warning(Id + _locale.FileSession.DisconnectForced);
            Send(statusCode);
            Disconnect();
        }

        protected override void OnReceived(byte[] buffer, long offset, long length)
        {
            switch (buffer[0])
            {
                case 0:
                {
                    if (Encoding.UTF8.GetString(buffer, (int)(offset + 1),
                        (int)(length - 1)) == _authenticationCode)
                    {
                        _isNotAuthenticated = false;
                        Send(new byte[] { 100 });
                        Logger.Information(Id + _locale
                            .FileSession.Authenticated);
                        return;
                    }
                    DisconnectForcefully(new byte[] { 198 });
                    break;
                }
                case 1:
                {
                    if (!_isNotAuthenticated)
                    {
                        var requestedFile = Path.Combine(_rootPath,
                            Encoding.UTF8.GetString(buffer, (int)(offset + 1),
                                (int)(length - 1)));
                        if (System.IO.File.Exists(requestedFile))
                        {
                            _requestedFile = requestedFile;
                            Send(new byte[] { 100 });
                            Logger.Information(Id + _locale
                                .FileSession.FileEntered);
                            break;
                        }
                        DisconnectForcefully(new byte[] { 199 });
                        break;
                    }
                    DisconnectForcefully(new byte[] { 198 });
                    break;
                }
                case 2:
                {
                    if (!_isNotAuthenticated)
                    {
                        if (_requestedFile != null)
                        {
                            var requestedFileLength = Encoding.UTF8.GetBytes(
                                new FileInfo(_requestedFile).Length.ToString());
                            var fileLengthBuffer = new byte[requestedFileLength.Length + 1];
                            fileLengthBuffer[0] = 101;
                            for (var i = 0; i < requestedFileLength.Length; i++)
                            {
                                fileLengthBuffer[i + 1] = requestedFileLength[i];
                            }
                            Send(fileLengthBuffer);
                            Logger.Information(Id + _locale
                                .FileSession.LengthRequested);
                            break;
                        }
                        DisconnectForcefully(new byte[] { 199 });
                        break;
                    }
                    DisconnectForcefully(new byte[] { 198 });
                    break;
                }
                case 3:
                {
                    if (!_isNotAuthenticated)
                    {
                        if (_requestedFile != null)
                        {
                            using var fileStream = System.IO.File.OpenRead(_requestedFile);
                            while (fileStream.Position < fileStream.Length)
                            {
                                var basicChunkBuffer = new byte[OptionSendBufferSize];
                                fileStream.Read(basicChunkBuffer);
                                Send(basicChunkBuffer);
                            }
                            fileStream.Close();
                            break;
                        }
                        DisconnectForcefully(new byte[] { 199 });
                        break;
                    }
                    DisconnectForcefully(new byte[] { 198 });
                    break;
                }
                case 4:
                {
                    if (!_isNotAuthenticated)
                    {
                        //TODO: Prevent Error if too many files are in one directory
                        if (length == 1)
                        {
                            var directoryItems = _fileSessionManager
                                .GetDirectoryItems(_rootPath);
                            var directoryItemsBuffer = new byte[directoryItems.Length + 1];
                            directoryItemsBuffer[0] = 104;
                            for (var i = 0; i < directoryItems.Length; i++)
                            {
                                directoryItemsBuffer[i + 1] = directoryItems[i];
                            }
                            Send(directoryItemsBuffer);
                            break;
                        }
                        var extendedDirectoryItems = _fileSessionManager
                            .GetDirectoryItems(_rootPath, Encoding.UTF8.GetString(
                                buffer, (int)offset + 1, (int)length - 1));
                        var extendedDirectoryItemsBuffer =
                            new byte[extendedDirectoryItems.Length + 1];
                        extendedDirectoryItemsBuffer[0] = 104;
                        for (var i = 0; i < extendedDirectoryItems.Length; i++)
                        {
                            extendedDirectoryItemsBuffer[i + 1] = extendedDirectoryItems[i];
                        }
                        Send(extendedDirectoryItemsBuffer);
                        break;
                    }
                    DisconnectForcefully(new byte[] { 198 });
                    break;
                }
                default:
                {
                    DisconnectForcefully(new byte[] { 199 });
                    break;
                }
            }
        }
    }
}
