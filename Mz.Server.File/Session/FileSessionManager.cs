﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Mz.Server.File.Session
{
    internal class FileSessionManager
    {
        public byte[] GetDirectoryItems(string rootPath, string extendedPath = "")
        {
            try
            {
                var directoryInfo = new DirectoryInfo(
                    Path.Combine(rootPath, extendedPath));
                var directoryItems = directoryInfo.
                    GetDirectories().Select(x => x.Name).ToList();
                directoryItems.AddRange(directoryInfo
                    .GetFiles().Select(x => x.Name));
                var directoryItemsString = string.Join(':', directoryItems);
                return Encoding.UTF8.GetBytes(directoryItemsString);
            }
            catch
            {
                return Encoding.UTF8.GetBytes(string.Empty);
            }
        }
    }
}
